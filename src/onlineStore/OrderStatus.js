import React from 'react';

function OrderStatus(props) {

    return (
        <div>
            {props.location.state.cardNo.includes('1234') ? <h1 style={{color: 'red'}}>Transaction failed</h1> :
                <h1>Dear valued customer {props.location.state.name} Thank you for purchasing from us</h1>}
        </div>
    );
}

export default OrderStatus;
