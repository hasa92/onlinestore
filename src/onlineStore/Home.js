import React, {useState, useEffect, useContext} from 'react';
import {OnlineStoreContext} from '../context/OnlineStoreContext'
import Item from "./Item";
import '../App.css';


function Home() {
    const [storeItems, setStoreItems] = useContext(OnlineStoreContext);
    const [isFetched, setIsFetched] = useState(false);

    useEffect(() => {
        fetchAllAvailableItems();
    }, [isFetched]);

    const fetchAllAvailableItems = () => {
        if (!isFetched) {
            setStoreItems([{
                id: 0,
                name: 'Mobile Phone',
                price: '920',
                image: 'mobilePhone',
                isAddedToTheStore: false,
                avilStocks: 23
            },
                {
                    id: 1,
                    name: 'TV',
                    price: '600',
                    image: 'Tv',
                    isAddedToTheStore: false,
                    avilStocks: 17
                },
                {
                    id: 2,
                    name: 'Refrigerator',
                    price: '800',
                    image: 'refrigerator',
                    isAddedToTheStore: false,
                    avilStocks: 18
                },
                {
                    id: 3,
                    name: 'External Hard Disk',
                    price: '45',
                    image: 'externalHardDrive',
                    isAddedToTheStore: false,
                    avilStocks: 40
                },
                {
                    id: 4,
                    name: 'Laptop',
                    price: '1000',
                    image: 'laptop',
                    isAddedToTheStore: false,
                    avilStocks: 5
                },
                {
                    id: 5,
                    name: 'Head phone',
                    price: '30',
                    image: 'headphone',
                    isAddedToTheStore: false,
                    avilStocks: 13
                },
                {
                    id: 6,
                    name: 'Sunglass',
                    price: '70',
                    image: 'sunglass',
                    isAddedToTheStore: false,
                    avilStocks: 15
                },
                {
                    id: 7,
                    name: 'Backpack',
                    price: '20',
                    image: 'backpack',
                    isAddedToTheStore: false,
                    avilStocks: 30
                },
                {
                    id: 8,
                    name: 'Wallet',
                    price: '10',
                    image: 'wallet',
                    isAddedToTheStore: false,
                    avilStocks: 20
                },
                {
                    id: 9,
                    name: 'Pen Drive',
                    price: '5',
                    image: 'penDrive',
                    isAddedToTheStore: false,
                    avilStocks: 50
                }
            ]);
        }

        setIsFetched(true);
    };

    return (
        <div className="section group">
            {storeItems.map(item => (
                <div key={item.id} className="col span_1_of_4">
                    <Item id={item.id} img={item.image} name={item.name} price={item.price} avilStock={item.avilStocks} isAddedToTheStore={item.isAddedToTheStore}/>
                </div>
            ))}
        </div>
    );
}

export default Home;