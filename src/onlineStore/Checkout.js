import React, {useContext, useState} from 'react';
import {OnlineStoreContext} from "../context/OnlineStoreContext";

function Checkout(props) {
    const [storeItems, setStoreItems] = useContext(OnlineStoreContext);
    const [name, setName] = useState('');
    const [cardNo, setCardNo] = useState('');
    const [errors, setErrors] = useState([
        {
            errors: {
                name: '',
                cardNo: ''
            }
        }
    ]);
    let total = 0;

    const handleSubmit = event => {
        event.preventDefault();
        props.history.push('/order-status', {
            cardNo: cardNo,
            name: name
        })
    };

    const handleChange = (event) => {
        event.preventDefault();
        const {name, value} = event.target;

        switch (name) {
            case 'name':
                setName(value);
                errors.name =
                    value.length < 2
                        ? 'Name must be more than one character'
                        : '';
                break;
            case 'cardNo':
                setCardNo(value);
                errors.cardNo =
                    value.length !== 16
                        ? 'Card no should be 16 characters long!'
                        : '';
            default:
                break;
        }

        setErrors([{errors, [name]: value}]);
    };

    return (
        <div className="checkout">
            <div className='wrapper'>
                <div className='form-wrapper'>
                    <h2>You are going to buy</h2>
                    <table style={{width: '50%', marginLeft: '25%'}}>
                        <tbody>
                        {storeItems.map(item => {
                            if (item.isAddedToTheStore) {
                                total = total + (item.price * (item.originalValue - item.avilStocks));
                                return (
                                    <tr key={item.id}>
                                        <td>
                                            <img className='image' src={require(`../asserts/${item.image}.png`)}/>
                                        </td>
                                        <td style={{textAlign: 'left'}}>{item.name} *
                                            ({item.originalValue - item.avilStocks})
                                        </td>
                                        <td style={{textAlign: 'right'}}>${item.price * (item.originalValue - item.avilStocks)}</td>
                                    </tr>
                                )
                            }
                        })}
                        <tr>
                            <td></td>
                            <td style={{textAlign: 'left'}}>Total</td>
                            <td style={{textAlign: 'right'}}>${total}</td>
                        </tr>
                        </tbody>
                    </table>
                    <form onSubmit={handleSubmit} noValidate>
                        <div className='name'>
                            <label htmlFor="name">Name</label>
                            <input className='form-input' type='text' value={name} name='name' onChange={handleChange}
                                   noValidate/>
                            {errors[0].errors.name ?
                                <span className='error'>{errors[0].errors.name}</span> : ''}
                        </div>
                        <div className='cardNo'>
                            <label htmlFor="cardNo">Card No</label>
                            <input className='form-input' value={cardNo} type='number' name='cardNo'
                                   onChange={handleChange}
                                   noValidate/>
                            {errors[0].errors.cardNo ?
                                <span className='error'>{errors[0].errors.cardNo}</span> : ''}
                        </div>
                        <div className='submit'>
                            <button disabled={name.length < 2 || cardNo.length !== 16} className='form-button'>Checkout
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Checkout;
