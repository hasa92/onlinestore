import React, {useContext, useState} from "react";
import '../App.css';
import {OnlineStoreContext} from "../context/OnlineStoreContext";

const Item = ({id, img, name, price, avilStock, isAddedToTheStore}) => {
    const [storeItems, setStoreItems] = useContext(OnlineStoreContext);
    const [itemCount, setItemCount] = useState(0);

    const addToCart = id => {
        let selectedItem = storeItems.find(obj => obj.id === id);

        if (selectedItem.avilStocks < itemCount) {
            alert('Cannot exceed available stocks');
            setItemCount(0);
        } else {
            let originalValue = selectedItem.avilStocks;
            selectedItem.avilStocks = selectedItem.avilStocks - itemCount;
            selectedItem.isAddedToTheStore = true;
            selectedItem.originalValue = originalValue;

            setStoreItems(prevItems => [...prevItems]);
        }
    };

    const removeFromCart = id => {
        let selectedItem = storeItems.find(obj => obj.id === id);
        selectedItem.avilStocks = selectedItem.originalValue;
        selectedItem.isAddedToTheStore = false;

        setStoreItems(prevItems => [...prevItems]);
        setItemCount(0);
    };

    const updateCount = e => {
        setItemCount(e.target.value);
    };

    return (
        <div className="item">
            <img className='image' src={require(`../asserts/${img}.png`)}/>
            <h3>{name}</h3>
            <p>price ${price}</p>
            <p>Available Stock(s) {avilStock}</p>
            <div className="qty">
                <input onChange={updateCount} type="number" min="1" max={avilStock} value={itemCount === 0 ? '' : itemCount} placeholder="Qty"/>
                <button disabled={itemCount === 0 || isAddedToTheStore} onClick={() => addToCart(id)}>Add to Cart</button>
                {isAddedToTheStore ? <button onClick={() => removeFromCart(id)}>Remove</button> : ''}
            </div>
        </div>
    );
};

export default Item;