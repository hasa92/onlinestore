import React from 'react';
import './App.css';
import Nav from './Nav';
import Home from "./onlineStore/Home";
import Checkout from './onlineStore/Checkout';
import OrderStatus from './onlineStore/OrderStatus';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {StoreItemProvider} from './context/OnlineStoreContext';

function App() {
    return (
        <StoreItemProvider>
            <Router>
                <div className="App">
                    <Nav/>
                    <Switch>
                        <Route path="/" exact component={Home}/>
                        <Route path="/checkout" component={Checkout}/>
                        <Route path="/order-status" component={OrderStatus}/>
                    </Switch>
                </div>
            </Router>
        </StoreItemProvider>
    );
}

export default App;
