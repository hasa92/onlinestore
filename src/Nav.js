import React, {useContext, useEffect, useState} from 'react';
import './App.css';
import {Link} from "react-router-dom";
import {OnlineStoreContext} from "./context/OnlineStoreContext";

const Nav = () => {
    const [storeItems, setStoreItems] = useContext(OnlineStoreContext);
    const [count, setCount] = useState(0);
    const [price, setPrice] = useState(0);

    const navStyle = {
        color: 'white'
    };

    useEffect(() => {
        let cnt = 0;
        let prce = 0;

        for (let obj of storeItems) {
            if (obj.isAddedToTheStore) {
                prce = prce + (obj.price * (obj.originalValue - obj.avilStocks));
                cnt++;
            }
        }

        setCount(cnt);
        setPrice(prce);
    }, [storeItems]);

    return (
        <nav>
            <Link style={navStyle} to={'/'}>
                <h3>Online Store</h3>
            </Link>
            <ul className="nav-links">
                { count > 0 &&
                <Link style={navStyle} to={'/checkout'}>
                    <li>Checkout({count})</li>
                </Link>
                }

                <li>Total Price ${price}</li>
            </ul>
        </nav>
    );
};

export default Nav;