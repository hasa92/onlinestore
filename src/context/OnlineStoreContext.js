import React, {useState, createContext} from "react";

export const OnlineStoreContext = createContext();

export const StoreItemProvider = props => {
    const [storeItems, setStoreItems] = useState([]);

    return (
        <OnlineStoreContext.Provider value={[storeItems, setStoreItems]}>
            {props.children}
        </OnlineStoreContext.Provider>
    )
};